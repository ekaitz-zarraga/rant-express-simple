import nunjucks from "nunjucks"
import express from "express"
import cookieParser from "cookie-parser"

let port = 8000;
let app = express();

// for HTML rendering in the server side
nunjucks.configure('templates', {
    autoescape: true,
    express: app
});
// for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// for parsing cookies
app.use(cookieParser());

let db = {
  credentials: {
    ekaitz: "password",
    ramon: "1234"
  },
  sessions: {},
  rants: []
}

app.get('/', (req, res)=>{
  console.log(db.rants);
  res.render("index.html", {rants: db.rants});
});

app.get('/login', (req, res) => {
  res.render("login.html");
});

app.post('/login', (req, res) => {
  if(req.body &&
     req.body.username in db.credentials &&
     req.body.password == db.credentials[req.body.username]){
    // Logged in successfully: store the session here hehe
    let session_id = "KOOKIEEE" + Math.random();
    db.sessions[session_id] = req.body.username;
    res.cookie("sessionId", session_id).render("new-rant.html");
  } else {
    res.status(401).send();
  }
});

app.post('/rant', (req, res)=>{
  if("sessionId" in req.cookies && req.cookies["sessionId"] in db.sessions){
    if(!req.body || !req.body.rant){
      res.status(400).send();
    } else {
      let id = db.rants.length;
      db.rants[id] =  { author: db.sessions[req.cookies["sessionId"]], rant: req.body.rant};
      res.render("success.html", { rant_url: "/rant/"+ id })
    }
  } else {
    res.status(401).send();
  }
});

app.get('/rant/:id', (req, res)=>{
  let id = req.params.id;
  let rant = db.rants[id];
  if (!rant){
    res.status(404).send();
  } else {
    res.render("rant.html", {rant_id: id, rant: rant.rant, author: rant.author});
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
